import watchdog.events
import watchdog.observers
import time
import sys
import subprocess

last_trigger_time = time.time()


def call_formatter(event_src_path):
    # Timing here is used to verify if the same path generates more than 1 event
    # Usually it is used 1s (file modification in windows seems to generate 3 events), so it prevents executing the
    # function too many times
    # Value was increased to 2s, so it is time enough for g-cli to run and do not trigger another event
    # Strategy could be changed to something else
    global last_trigger_time
    current_time = time.time()
    if event_src_path.find("~") == -1 and (current_time - last_trigger_time) > 2:
        last_trigger_time = current_time
        print("Watchdog received event - % s." % event_src_path)
        # Assume Format VI is in G-CLI plugins folder
        subprocess.call(["g-cli.exe", "Format", "--", event_src_path])


class Handler(watchdog.events.PatternMatchingEventHandler):
    def __init__(self):
        # Set the patterns for PatternMatchingEventHandler
        watchdog.events.PatternMatchingEventHandler.__init__(
            self, patterns=["*.vi"], ignore_directories=True, case_sensitive=False
        )

    def on_created(self, event):
        call_formatter(event.src_path)

    def on_modified(self, event):
        call_formatter(event.src_path)


if __name__ == "__main__":
    # Process arguments
    src_path = sys.argv[1] if len(sys.argv) > 1 else "."

    event_handler = Handler()
    observer = watchdog.observers.Observer()
    observer.schedule(event_handler, path=src_path, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
